const getSum = (str1, str2) => {
  let regexp = /[\D]+$/;
  let convertedStr1;
  let convertedStr2;
  let result;
  if(typeof str1 !== 'string' || typeof str2 !== 'string' || regexp.test(str1) || regexp.test(str2)) {
      return false;
  }
  if(str1 === '') {
    str1 = 0;
  } else if(str2 === '') {
      str2 = 0;
  }
  
  convertedStr1 = parseInt(str1);
  convertedStr2 = parseInt(str2);
  

  result = convertedStr1 + convertedStr2;

  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let commentCount = 0;
  for(const element of listOfPosts) {
    if(element['author'] === authorName) {
      postCount++;
    }

    if(element.hasOwnProperty('comments')) {
      for(let j = 0; j < element['comments'].length; j++) {
        if(element['comments'][j]['author'] === authorName) {
            commentCount++;
        }
      }
    }
  }
  return `Post:${postCount},comments:${commentCount}`;
};

const tickets = (people) => {
  let moneyVasya = 0;
  let ticketCost = 25;

  for(const element of people) {
      if(+element === 25) {
          moneyVasya += ticketCost;
      } else {
            let difference = Math.abs(+element - moneyVasya);
            if(moneyVasya > +element || difference <= moneyVasya && difference <= 25) {
                moneyVasya += ticketCost;
            } else {
                return 'NO';
            }
        }
    }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
